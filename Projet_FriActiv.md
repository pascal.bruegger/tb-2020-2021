---
version: 2
type de projet: Projet de Bachelor
année scolaire: 2020/2021
titre: FriActiv - App Mobile pour la promotion du mouvement et du tourisme
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - Service du Sport de l'état de Fribourg
professeurs co-superviseurs:
  - Non attribué
proposé par étudiant: Jérémie Rossier  
mots-clé: [GPS, Application Mobile, Sport]
langue: [F,E]
confidentialité: non
suite: oui
attribué à: [Jérémie Rossier]
instituts:
- iCoSys
---

## Description
Le projet « FriActif », proposé par le service des sports, a pour but de développer l’activité physique dans toute la population et de valoriser les lieux de vie et les espaces touristiques (urbains) du canton de Fribourg.
En s’appuyant sur le Concept cantonale du sport, « FriActif » vise à augmenter la part de la population active en ciblant toutes les tranches d’âges et d’exploiter les synergies avec le tourisme. Le SSpo et l’UFT collaborent avec les villes et régions, les associations populaires (Pro Senectute, Pro Juventute, Associations des quartiers) et les clubs de sport ou écoles intéressés, afin d’élaborer une offre de mouvement attrayante et diversifiée qui réponds aux besoins de notre large panel d’habitants. « FriActif » se veut une offre local et régional, installé au niveau cantonal pour rayonner au-delà de ces frontières.
Basé sur un pré-projet de semestre dont l'étudiant a implémenter un POC (Proof of concept), l’application « FriActif » sera l’outil principal qui guidera le client sur un parcours choisit. L’utilisateur suit un tracé GPX/GPS prédéfinit. L’App lui indique l’emplacement du prochain poste. Arrivé à l’emplacement l’App lui indique l’exercice à faire et lui fournit des informations touris-tiques sur l’emplacement. 
L’application doit permettre différents choix de parcours, de catégories d’exercices et de difficul-té.
Idéalement l’utilisateur peut recourir à un service « coach » qui lui permet de progresser ou de définir des objectifs personnalisés (temps de parcours, nombre de répétitions dans un exercice ou difficulté d’un exercice).
Comme le service des sports a mandaté une entreprise pour industrialiser cette application l'étudiant devra gèrer  

## Contraintes (négociables)
L’application doit fournir de multiples services :
1)	Parcours GPX :
-	2-4 parcours prédéfinis dans cinq villes fribourgeoises
-	Emplacements de postes
-	Accompagnement de l’utilisateur sur le tracé
2)	Choix de départ :
-	Choix de parcours (long- court, semi-urbain – urbain, tradition – bâtiment – historique – culturel – géographie - didactique…)
-	Choix d’utilisation : famille – curieux – groupe – athlète
-	Choix d’activité : Agile – Respire – Tonic
-	Choix rapide : Selon choix précédents
3)	Postes :
-	Images et films de présentation
-	Texte descriptif (exercice d’activité physique + tourisme)
-	Propositions de progression ou corrections individualisables
4)	Choix de poste :
-	Changer la présélection par poste (échappatoire ou adaptation personnelle)
5)	Entretien :
-	De nouvelles villes et nouveaux parcours doivent pouvoir être ajouté au bout d’une ou plusieurs années.
-	L’application doit rester fonctionnel et être améliorée sur 5-10 ans

## Objectifs/Tâches

- Elaboration d'un cahier des charges.
- Analyse des applications existances et des technologies possibles pour cette applications au niveua des plateformes de développement (Android, iOS, Xamarin, Flutter).
- Analyse des technologies backend.
- Conception et implémentation d'une application mobile avec les contraintes ci-dessus 
- Conception et implémentation d'un backend avec la base de donnée contenant les comptes utilisateurs et les parcours  
- Tests et validation du système.
- Rédaction d'une documentation adéquate.

