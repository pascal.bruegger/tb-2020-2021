---
version: 2
type de projet: Projet de Bachelor
année scolaire: 2020/2021
titre: HMMA 2 - Health Minitoring Mobile APP
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - Pascal Bruegger
professeurs co-superviseurs:
  - Non attribué
mots-clé: [Flutter, Xamarin, Android, iOS, Mobile App]
langue: [F,E]
confidentialité: non
suite: oui
instituts:
- iCoSys
---

## Description
Le maintien à domicile des personnes âgées est devenu un sujet sociétal et la problématique est complexe. Comment garder nos ainés le plus longtemps possible dans leur propre lieu de vie tout en garantissant une qualité de vie la meilleure et la plus sécurisée possible. 
Dans la société dans laquelle nous vivons, ces personnes se sentent souvent seules et quelque peu coupée de tout liens sociaux et avec l’arrivée de la COVID-19, ce sentiment s’est encore renforcé. Nous avons vu l’augmentation de l’utilisation des nouvelles technologies (smartphones et tablettes) afin de garder un contact avec l’extérieur (famille, soins à domicile).
Destinée au personnel éducatif et soignant, l'application reçoit des informations transmises du serveur SEMS (Smart Environment Monitoring system) et permet de suivre en temps réel une personne au niveau de ses données physiologiques, de son activité, d'une chute et éventuellement de sa localisation. Elle permet aussi de valider des alertes déclenchées par le serveur et d’échanger des messages avec les personnes agées ayant l’application RMMA (Resident Monitoring Mobile App). Un système de notifications de prise en charge d'intervention sera mis en place.

## But du projet
Basée sur un premier prototype, le projet consiste en mise en place d’un système avec une application mobile pour le personnel soignant, interagissant avec un serveur SEMS qui lui centralise les informations des résidents. Cette application a pour but de recevoir les alarmes transmises par le serveur, d’en faire une notification sonore contextualisée à l’infirmière, l’aide-soignante, l’éducatrice, voir le médecin. La contextualisation se fera à travers des règles comme celle qui consiste à choisir la bonne personne en fonction de la gravité du cas : si le cas n’est pas grave alors le ou la référente du résident sera contacté et s’il y a urgence alors la personne la plus proche physiquement sera notifiée. Un serveur sera mis en place pour la gestion et la coordination des différents résidents. Il permettra de recevoir et d’envoyer des requêtes http à l’applications mobile et centralisera les données par résidents. Ce projet fait partie d’un projet plus vaste qui combinera 2 applications mobiles et serveur: HESTIA.

## Contraintes (négociables)
- L'application sera réalisée avec la technologie mutli-plateforme (Flutter, Xamarin)
- Elle sera connectée avec le serveur SEMS et pourra interagir avec l'app RMMA
- Elle devra être simple et érgonomique
- Elle respectera les standars graphiques iOS et Android.

## Objectifs/Tâches
- Elaboration d'un cahier des charges
- Analyse des applications existance
- Analyse des besoins du personnel médical ainsi des familles
- Choix d'une technologie (plateformes et langages)
- Réalisation de l'app mobile (Xamarin, Flutter)
- Tests et validation du système.
- Rédaction d'une documentation adéquate.
