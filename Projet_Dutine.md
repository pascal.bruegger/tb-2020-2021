---
version: 2
type de projet: Projet de Bachelor
année scolaire: 2020/2021
titre: Dutine - Gestionnaire de tâches
filières:
  - Informatique
nombre d'étudiants: 1
mandants:
  - Pascal Bruegger
professeurs co-superviseurs:
  - Non attribué
proposé par étudiant: Léonard Noth   
mots-clé: [GPS, Application Mobile, Sport]
langue: [F,E]
confidentialité: non
suite: oui
attribué à: [Léonard Noth]
instituts:
- iCoSys
---

## 1. Problématiques

Dans notre société actuelle, il est courant que des personnes doivent effectuer des tâches répétitives et peu intéressantes pour une tierce personne dans leur vie de tous les jours. 
Ces dites tierces personnes, ont généralement envie de pouvoir suivre l’évolution de la tâche qu’ils ont donné à faire, et aimeraient s’assurer que cette tâche sera effectuée dans les temps sur le long terme.

Dans ce document, nous utiliserons le terme donneur pour la personne qui attribue les tâches et le terme receveur pour celle devant les effectuer.

Dans ce genre de situation, deux problèmes majeurs se posent : 
1.) Comment garder une trace de l’historique de ces tâches ?
2.) Comment s’assurer que les receveurs, vont bien les effectuer dans les temps et qu’elles vont rester motiver sur le long terme ?

Le premier problème, lorsqu’il est « résolu », donne souvent lieu à l’utilisation d’une feuille de papier avec une liste des tâches ou le donneur va garder le compte des tâches qui ont été effectuées. 
Cependant, comme il est simple de perdre cette feuille et/ou que le receveur oublie de notifier le donneur, ce problème est souvent un obstacle trop important, et cela donne lieu à un suivi très approximatif voire pas de suivi du tout.

Le second problème est généralement simplement déplacé du côté du receveur.
Le donneur considère souvent que c’est le problème du receveur de rester motivé et ce problème n’est donc généralement pas réglé et entraine une baisse de motivation sur le long terme.

## 2. Présentation du projet

2.1.Concept général du projet
Comme l’a laissé sous-entendre le point 1, Dutine est une application mobile cross-plateforme qui permettra de suivre des tâches récurrentes sur le long terme qui seront attribuées à certains utilisateurs. 

Le fait que cette solution soit une application mobile cross-plateforme et qu’elle offre la possibilité que chaque tâche soit être validée par le donneur ou le receveur, permet de régler le premier problème mentionné au point 1.
En effet, comme toutes les informations sont stockées dans le cloud, il sera quasiment impossible de perdre les informations relatives au suivi d’une tâche. 
De plus, le fait que les deux partis puissent valider une tâche permet de s’assurer, de par la relation de confiance établie entre le donneur et le receveur, que le suivi est le plus précis possible.
Enfin, comme l'application offrira un historique pour chaque tâche qu’elle contient, il sera facile pour le donneur de voir comment se comporte le receveur vis-à-vis des tâches qu’il a à faire ce qui permet de garantir une transparence totale et un meilleur suivi des tâches.

Afin de régler le second problème mentionné dans le point 1, l'application est basée sur un système de récompenses et de sanctions. Si une tâche est complétée (ou partiellement complétée), le donneur reçoit une récompense et si elle n’est pas complétée dans les délais fixés par le donneur, alors le receveur est sanctionné. Cette démarche est plutôt commune, mais l’aspect innovant, est le fait que le donneur définit une liste de sanctions et de récompenses directement dans l’application. Il peut ensuite associer une sanction à une tâche et un nombre de points reçus si la tâche est complétée ou si le receveur à effectuer la tâche une fois. Une fois que le receveur a reçu des points, il peut les utiliser pour « acheter » des récompenses appartenant à la liste des récompenses définie par le donneur. De ce fait, le receveur est plus motivé à effectuer ses tâches car il peut choisir sa récompense en fonction de ce qu’il veut sur le moment.

Cette application est une application qui peut être utilisée dans tous les domaines ou un suivi de tâches récurrentes est nécessaire comme: 
•	Pour une secrétaire qui doit prendre le courrier au moins deux fois par semaine et répondre aux emails au moins une fois par jour. Elle pourrait par exemple être récompensée en pouvant acheter grâce aux points obtenus une après-midi de congé.
•	Pour un employé dans un supermarché qui doit remplir les rayons deux fois par semaine et qui doit nettoyer le magasin 2 fois par mois. Il pourrait par exemple acheter le fait de ne pas devoir nettoyer le magasin durant un mois.
•	Dans une famille, ou un enfant devrait débarrasser le lave-vaisselle au moins 3 fois par semaine. Et il pourrait par exemple acheter grâce aux points une certaine somme d’argent de poche par exemple.

## 3. Fonctionnalités et technologies

3.1.Fonctionnalités 

- Un système de connexion / création de compte
- Un système de relation entre utilisateurs (qui est le donneur pour quel receveur) sous la forme d’invitation pouvant être refusée ou acceptée
- Un système permettant à un utilisateur d’avoir plusieurs donneurs et d’être le donneur pour quelqu’un et le receveur pour quelqu’un d’autre
- Un système de fonctions dans le cloud qui se lance une fois par heure et qui vérifie les tâches qui sont échues et ajoute les punitions correspondant à ces dernières pour les receveurs concernés•Un système de création de punitions et de récompenses•Un système de création de tâches (avec tous les éléments mentionnés dans le point 2.2)
- Un système permettant au receveur et au donneur de valider une tâche•Un système de notes/commentairepermettant d’ajouter une note/un commentaire(des règles par exemple) pour une relation donneur/receveur
- Un système d’ajout automatique des points lorsqu’une tâche est complétée et/ou avancée (une itération est effectuée)
- Un système d’achat ou d’échangede récompenses par un receveur•Un système d’ajout et de retrait manuel de punitions par un donneur

3.2.Fonctionnalités nice-to-have
- Une application multilingue et pas uniquement en anglais
- Un système d’ajout / retrait manuel de points (permettant d’acheter une récompense)
- Un système d’abonnement Freemium (pour débloquer un nombre illimité de tâches par exemple)
- Un système de notification permettant d’informer les utilisateurs si une tâche doit bientôt être terminée, si une punition a été ajouté ou qu’une récompense aété achetée•Un système de Dashboardet de rapport journaliers/hebdomadaires, etc.

3.3.Technologies proposées
- Flutter: Développement d’une application cross-plateforme•Firebase: Authentification pour la gestion de compte et la connexion
- Firebase Firestore: Stockage des différents éléments relatifs à l’application dans la base de donnéeset la persistance des données sur l’appareil mobile
- Firebase function: Pour les fonctions qui doivent être exécutée automatiquement toutes les heures

## 4. Objectifs/Tâches

- Elaboration d'un cahier des charges.
- Analyse des applications existances et des technologies possibles pour cette applications au niveua des plateformes de développement (Android, iOS, Xamarin, Flutter).
- Conception et implémentation d'une application mobile avec les contraintes ci-dessus
- Tests et validation du système.
- Rédaction d'une documentation adéquate.
