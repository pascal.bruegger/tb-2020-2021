---
version: 2
type de projet: Projet de Bachelor
année scolaire: 2020/2021
titre: Name In Progress - App mobile de follow up de formation proffessionelle
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - L’Institut Européen de Coopération et Développement (IECD) et Semeurs d’avenir (Liban)
professeurs co-superviseurs:
  - Non attribué
mots-clé: [Application Mobile, Backend, ONG, formation professionnel]
langue: [F,E]
confidentialité: non
suite: non
instituts:
- iCoSys
---

## Contexte
L’Institut Européen de Coopération et Développement (IECD) et Semeurs d’avenir, son
partenaire au Liban, est une organisation non-gouvernementale focalisée sur l’acquisition et
le développement de compétences professionnelles chez les jeunes en situation de
vulnérabilité.
L’IECD travaille sur deux modèles de formation professionnelle : les formations longues et les
formations courtes.
Dans le premier modèle, l’IECD intervient auprès de la Direction Générale de l’Enseignement
technique et Professionnel du Liban où il gère trois filières de Bac Technique :
- Informatique
- Maintenance
- Électrotechnique
L’IECD est en charge du développement des curriculums d’enseignements, du déroulement
des formations, ainsi que du suivi des enseignants.
Le deuxième modèle propose des formations non-formelles de courte durée, permettant aux
jeunes d’acquérir les compétences de bases nécessaires pour travailler dans un domaine
précis.

## Problématique perçue
Aujourd’hui, les formateurs et les responsables de projets IECD, communiquent entre eux ainsi
qu’avec les étudiants via whatsapp. La communication se fait uniquement dans un sens et
c’est principalement les responsables de projets IECD qui sont actifs. L’IECD n’a pas toujours les
moyens de suivre les formateurs dans leurs cours et de capter leurs besoins et ceux des jeunes.
Un outil centralisé, facile d’utilisation, dont les informations seraient directement visualisées
permettrait d’améliorer l’impact en augmentant la réactivité de l’équipe. De plus, une fois les
données rendues compréhensibles, elles deviendront un support managérial efficient lors de
l’évaluation de l’impact terrain ainsi que dans la négociation avec les bailleurs de fonds.

## Description
En finalité, le projet d’application mobile de follow up vise à collecter et visualiser les différentes
informations en lien avec les activités journalières des bénéficiaires des formations organisées
par l’institut Européen de Coopération et Développement – Semeurs d’avenir, au Liban.
Les données récoltées porteront sur divers niveaux en fonction des utilisateurs. Elles
présenteront :
- Les niveaux de compétences par étudiants / par formation
- Le parcours chronologique d’acquisition des compétences / de ressenti
- Les besoins décelés
- Les points faibles et points forts des étudiants / des formations
Utilisateurs étudiants :
Les étudiants pourront évaluer leur ressenti et leur niveau perçu d’acquisition des compétences
travaillées à la fin de chaque cours.
Ils recevront également le feedback de leur formateur.
A la fin de la formation, une série de questions leur sera posée afin de déterminer son état
d’esprit quant à son niveau de compétence et son bien-être, mais aussi, permettra d’évaluer
de nouveaux besoins.
Utilisateurs formateurs :
Les formateurs recevront une alerte les jours de leur cours. Ils devront sélectionner, depuis une
liste proposée, les compétences qu’ils vont travailler ce jour-là avec la classe. Pendant la
formation, ou peu après, le formateur pourra évaluer le niveau perçu de compétence atteint
par chaque étudiant.
Ils recevront également un feedback journalier de leur cours par leurs étudiants.
A la fin de la formation, une série de questions leur sera proposée afin de déterminer leur état
d’esprit quant à cette session de formation et leur bien-être mais aussi, permettra d’évaluer de
nouveaux besoins.
Utilisateurs IECD :
Le personnel IECD pourra suivre en direct l’acquisition des données et leurs projections. Il pourra
transmettre et présenter les données lors de discussion avec le siège, d’autres délégations ou
encore des bailleurs de fonds, à des fins d’évaluation ou d’analyses.
Il sera en charge d’entrer les données nécessaires au bon fonctionnement de l’application.

**Récolte de données**
L’évaluation se ferait de manière simple et visuelle :
- Évaluation quantitative :
3 niveaux de satisfactions au choix.
- Évaluation qualitative :

**Propositions d’énoncés**
Utilisation générale – Points de vigilance à réfléchir
Afin de préserver la santé mentale et la bonne relation des étudiants et des formateurs, il est
important de penser à un dispositif de préservation en cas de feedback négatif.
Possibilité d’intégrer une forme de gamification positive.

## Contraintes (négociables)
L’application doit pouvoir :

Après un cours :

- Recueillir les réactions:

1. Des étudiants

1. Des formateurs
- Recueillir le taux de participation au cours
- Recueillir le niveau de compétence perçu des étudiants, des formateurs

Après la formation:
- Capter le ressenti final des étudiants (je me sens capable de…)
- Capter le ressenti final des formateurs
- Proposer de nouveaux besoins

Interface:
- Organiser les données de manière graphique immédiatement.
- Facile à utiliser, très graphique, visuelle.

Utilisateurs: 
Peut être utilisé par tout le monde mais à des niveaux différents :
- Niveau managérial avec accès à toutes les données
- Niveau formateur avec accès aux données liées à ses interventions
- Niveau étudiant avec son parcours / évolution

Résultat final: 

- _Pédagogique_

Une application permettant de consigner la progression des étudiants de manière simple,
sans surcharge de travail pour les enseignants. Elle permettra également de suivre la qualité
de l’accompagnement que les formateurs offrent à leurs étudiants.

- _Managérial_

L’application permettra de déceler les besoins des bénéficiaires (formateurs et étudiants)
afin de les combler dans la mesure du possible. Elle permettra également de visualiser
clairement l’impact des projets et deviendra une base de dialogue avec les bailleurs de fond
de l’ONG.

## Objectifs/Tâches

**Il est clair que ce projet est ambitieux et nécéssitera plus qu'un travail de bachelor pour être fonctionnel. Le but est de démontrer la faisabilité en implémentant certaines des nombreuses foctionnalités décrites. L'étudiant aura la responsabilité de définir les limites de son projet.**

Les objectifs du travail seront:

- Elaboration d'un cahier des charges.
- Analyse des applications existances 
- Analyse des technologies front (Android, iOS, Flutter, etc.) et backend.
- Conception et implémentation d'une application mobile avec les contraintes ci-dessus 
- Conception et implémentation d'un backend avec la base de donnée contenant toutes les informations requises 
- Tests et validation du système.
- Rédaction d'une documentation adéquate.
