---
version: 2
type de projet: Projet de Bachelor
année scolaire: 2020/2021
titre: Web Agenda - prise de rendez-vous en ligne
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - Pascal Bruegger
professeurs co-superviseurs:
  - Non attribué
proposé par étudiant: Simone Quirici  
mots-clé: [Web technologie, MySQL, Front/back end]
langue: [F,E]
confidentialité: non
suite: oui
attribué à: [Simone Quirici]
instituts:
- iCoSys
---


## Description
Vous êtes patient d'un cabinet de physiothérapie ou d'Ostéopathie et vous devez systématiquement prendre rendez-vous par téléphone. Pourquoi ne pas prendre vos rendez-vous en ligne? Avoir accès aux plages libres afin de prendre rendez-vous serait un plus pour les cabinets: moins de travail pour les secrétaires. Tout comme les déplacements de rendez-vous d'expértise au service des automobiles, il suffit de ce rendre sur le site web de l'OCN, rentré son n° de convocation et il est possible de changer son rendez-vous.
Après un premier POC (roof of concept), le but de ce projet est de définir le concept dans sa globalité et de créer un agenda web pour saisir des rendez-vous par les patients d'un cabinet médical, physio, osteo ou toute autre institution. Des questions comme "qui a le droit de prendre un rendez-vous?", "choix du thérapeute ou non?", la validation des rendez-vous et la notificaion au patient devront être abordées. Une attention particulière devra mise l'interface utilisateur afin de garantir une utulisation simple, efficace et logique. De plus, le concept se veut générique avec la possibilité de s'adapter dans un site web existant comme nouvelle page et fonctionnalité (éventuellement sous forme de plugin WordPress) afin d'offrir aux cabinets une intégration du Web Agenda dans leur site web. De plus, il devra interfacer avec une base de donnée de MySQL gêrée et utilisée par un logiciel de gestion de cabinet Physio-Ostéopathie. 

## Contraintes (négociables)
- Utilisation des techologies Web récentes
- La page Web Agenda devra pouvoir s'adapter à des sites web existants
- La gestion des droits sera au centre du projet (qui a le droit de prendre un rendez-vous, choix de son thérapeute ou non, etc. ?) 
- Interfaçage avec une base de donnés MySQL (cloud ou sur un NAS) et notament une table rendez-vous

## Objectifs/Tâches
- Elaboration d'un cahier des charges
- Veille technologique sur les logiciels et solution web existantes.
- Proposition d'un concept
- Choix d'une technologie pour le front-end et eventuellement adaptation du back-end
- Implémentation de la solution proposée
- Tests et validation du système
- Rédaction d'une documentation adéquate

## Liens utiles
- https://www.calendoc.com/besoin-personalise/?gclid=Cj0KCQiAmL-ABhDFARIsAKywVaektqDsfVxWZmYzz5DrxbtpIyw5TLmK41X_6ZjHe9u_ZBC4U4ptYeUaApViEALw_wcB
- https://www.timify.com/fr-ch/features/reservation-en-ligne/
- https://www.onedoc.ch/fr/
- https://www.codeur.com/blog/prise-rendez-vous-en-ligne/
- https://agenda.ch/

