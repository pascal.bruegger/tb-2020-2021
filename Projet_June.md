---
version: 2
type de projet: Projet de Bachelor
année scolaire: 2020/2021
titre: June - Développement d'un jeu mobile
filières:
  - Informatique
nombre d'étudiants: 1
mandants:
  - Pascal Bruegger
professeurs co-superviseurs:
  - Non attribué
proposé par étudiant: Mathieu Buchs   
mots-clé: [Jeu, Mobile, Game Design]
langue: [F,E]
confidentialité: non
suite: oui
attribué à: [Mathieu Buchs]
instituts:
- iCoSys
---

## Contexte
Le projet “June” a pour but de développer un jeu sur dispositif mobile qui serait ultimement déployé sur les plateformes habituelles, telles que le Google Play Store ou le iOS app Store. Ce projet s’inscrit dans la suite des cours de Game Design, d’applications mobiles et d’IT Startup Bootcamp. 
Le but est de parfaire les compétences de Game Design de l’étudiant, dans l’optique de poursuivre une carrière dans ce sens dans le futur. 
On désire à la fois proposer une idée fun et engageante pour le joueur, ainsi que travailler l’aspect économique du jeu.

## Présentation du projet
Le but de ce projet est de développer un jeu sur smartphone et/ou tablette. L’idée de base pour l’étudiant serait de développer un jeu de stratégie/simulation/gestion, du type de Farmville ou Plague Inc, tous deux ayant été des jeux très populaires.
Le scénario et le titre du jeu seront aussi déterminés durant le projet.
Un soin particulier sera donné à l’équilibrage, de sorte à optimiser la durée de vie du jeu et l’expérience du joueur, de même qu’aux finitions du produit.
Les assets graphiques seront également développés par l’étudiant.
L’aspect multijoueur n’est pas pour l’instant pas envisagé, mais est négociable.

## Objectifs/Tâches
- Analyse du marché actuel des jeux mobiles
- Analyse de la technologie (Unity, Unreal engine, autre?)
- Analyse du modèle économique (paiements in-app via un shop, publicités, etc.)
- Elaboration d’un cahier des charges
- Conception de maquettes puis d'un minimum viable product
- Playtesting et adaptations
- Processus itératif
- Déploiement de l’application sur le Google Play Store et/ou l’iOS App Store
